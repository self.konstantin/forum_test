export const state = () => ({
  forums: [],
  count: 0,
})

export const mutations = {
  forums(state, value) {
    state.forums = Array.from(value)
  },
  count(state, value) {
    state.count = value
  },
}

export const actions = {
  async loading({ commit }, page) {
    const currentPage = page || 1
    const url = `api/forum/?page=${currentPage}`
    const response = await this.$axios.get(url)
    const forums = response?.data?.results
    const count = response?.data?.count
    commit('forums', forums)
    commit('count', count)
  },
  async loadingForum({ commit }, slug) {
    const url = `api/forum/${slug}`
    const response = await this.$axios.get(url)
    const forums = response?.data?.tracks
    commit('forums', forums)
  },
}

export const getters = {
  forums(state) {
    return state.forums
  },
  count(state) {
    return state.count
  },
  total_page(state) {
    if (state.count) {
      return state.count / 10
    }
    return 1
  }
}
