from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class Config(AppConfig):
    label = 'app api'
    name = 'apps.api'
    verbose_name = _('API backend')
