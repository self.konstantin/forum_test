from django.urls import path, include
from rest_framework import routers

from apps.forum import views as forum_views


router = routers.DefaultRouter()
router.register(r'forum', forum_views.ForumViewSet)


urlpatterns = [
    path('', include(router.urls)),
]
