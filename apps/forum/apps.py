from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class Config(AppConfig):
    label = 'app forum'
    name = 'apps.forum'
    verbose_name = _('forum')

    def ready(self):
        import apps.forum.signals
