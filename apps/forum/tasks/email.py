import logging
from django.core import mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from celery import shared_task
from machina.core.db.models import get_model


ForumReadTrack = get_model('forum_tracking', 'ForumReadTrack')


@shared_task
def submit_forum_track(track_id):
    try:
        track = ForumReadTrack.objects.get(pk=track_id)
        if track.user.email:
            subject = 'New track'
            html_message = render_to_string('submit_forum_track.html', {
                'user': track.user,
                'forum': track.forum,
            })
            plain_message = strip_tags(html_message)
            from_email = 'From <from@example.com>'
            to = 'to@example.com'
            mail.send_mail(subject, plain_message, from_email, [to, track.user.email], html_message=html_message)

    except ForumReadTrack.DoesNotExist:
        logging.warning('Error send email ForumReadTrack')
