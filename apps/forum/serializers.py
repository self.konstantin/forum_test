from rest_framework import serializers
from machina.core.db.models import get_model


ForumReadTrack = get_model('forum_tracking', 'ForumReadTrack')
Forum = get_model('forum', 'Forum')

class ForumSLugSerializer(serializers.ModelSerializer):
    class Meta:
        model = Forum
        lookup_field = 'slug'
        fields = (
            'name',
            'slug',
        )

class ForumReadTrackSerializer(serializers.ModelSerializer):
    user_name = serializers.CharField(source='user', read_only=True)
    forum = ForumSLugSerializer(read_only=True)

    class Meta:
        model = ForumReadTrack
        fields = (
            'user_name',
            'forum',
            'mark_time',
        )


class ForumSerializer(serializers.ModelSerializer):
    tracks = ForumReadTrackSerializer(many=True, read_only=True)

    class Meta:
        model = Forum
        lookup_field = 'slug'
        fields = (
            'name',
            'slug',
            'tracks',
        )

