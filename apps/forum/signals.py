from django.db import transaction
from django.db.models.signals import post_save
from django.dispatch import receiver
from machina.core.db.models import get_model

from apps.forum.tasks import submit_forum_track


ForumReadTrack = get_model('forum_tracking', 'ForumReadTrack')


@receiver(post_save, sender=ForumReadTrack)
def create_profile(sender, instance, created, **kwargs):
    if created:
        transaction.on_commit(lambda: submit_forum_track.delay(instance.pk))

