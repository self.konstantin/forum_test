from rest_framework import permissions
from rest_framework import viewsets, filters, mixins
from django_filters.rest_framework import DjangoFilterBackend
from machina.core.db.models import get_model

from . import serializers


ForumReadTrack = get_model('forum_tracking', 'ForumReadTrack')
Forum = get_model('forum', 'Forum')


class ReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.method in permissions.SAFE_METHODS


class ForumReadTrackViewSet(viewsets.ModelViewSet):
    queryset = ForumReadTrack.objects.all()
    serializer_class = serializers.ForumReadTrackSerializer
    filter_backends = (DjangoFilterBackend,)
    permission_classes = (ReadOnly,)


from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response


class ForumViewSet(viewsets.ModelViewSet):
    queryset = ForumReadTrack.objects.all()
    serializer_class = serializers.ForumReadTrackSerializer
    filter_backends = (DjangoFilterBackend,)
    permission_classes = (ReadOnly,)
    lookup_field = 'slug'

    def retrieve(self, request, slug=None):
        queryset = Forum.objects.all()
        forum = get_object_or_404(queryset, slug=slug)
        serializer = serializers.ForumSerializer(forum)
        return Response(serializer.data)

